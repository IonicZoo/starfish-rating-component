[![npm](https://img.shields.io/npm/v/@ioniczoo/starfish-rating-component.svg)](https://www.npmjs.com/package/@ioniczoo/starfish-rating-component)
[![npm](https://img.shields.io/npm/dt/@ioniczoo/starfish-rating-component.svg)](https://www.npmjs.com/package/@ioniczoo/starfish-rating-component)
[![npm](https://img.shields.io/npm/l/@ioniczoo/starfish-rating-component.svg?style=flat-square)](https://www.npmjs.com/package/@ioniczoo/starfish-rating-component)

<img src="https://gitlab.com/IonicZoo/starfish-rating-component/raw/master/img.jpg" width="20%" height="auto" alt="starfish" title="starfish">

# Starfish Rating Component

Rating Component for Ionic: Our bright starfish will help you carry out rating. Our Starfish is a Rating Component.


## Install

```bash
npm install @ioniczoo/starfish-rating-component --save
```

## Import `RatingComponent` to  `home.module.ts`

```ts
...
import { RatingComponent } from '@ioniczoo/starfish-rating-component';

@NgModule({
  declarations: [
    HomePage,
    MapComponent,
    RatingComponent
  ],
  imports: [
    IonicPageModule.forChild(HomePage)
  ],
})
export class HomePageModule {}
```

## Example

```html
<ion-content>
  <ion-card padding text-center>
    My Rating: {{star | json}}
  </ion-card>
  <ion-list>
    <ion-item>
      <span zoo-rating-title>Rate the trip?</span>
      <zoo-rating name="starts" id="starts" icon="star" color="yellow" background="#ccc" [(ngModel)]="stars"></zoo-rating>
    </ion-item>
  </ion-list>
</ion-content>
```

## Author

[André Argôlo](https://argolo.gitlab.io)

## Contribute

[Create issues and request pull-request.](https://gitlab.com/IonicZoo/starfish-rating-component/blob/master/CONTRIBUTING.md)

## License

[GNU General Public License v3.0](https://gitlab.com/IonicZoo/starfish-rating-component/blob/master/LICENSE)
