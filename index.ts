import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { forwardRef } from '@angular/core';
import { EventEmitter } from '@angular/core';

import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@angular/forms';


@Component({
  selector: 'zoo-rating',
  template: `<ion-icon [name]="icon" (tap)="setStar(star)" [ngStyle]="{'color': value >= star + 1 ? color : background }" *ngFor="let _ of items; let star = index"></ion-icon>`,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => RatingComponent),
    multi: true
  }]
})
export class RatingComponent implements ControlValueAccessor {

  @Output() change: EventEmitter<any> = new EventEmitter();

  @Input() value: number = 5;
  @Input() stars: number = 5;
  @Input() color: string = '#f1f100';
  @Input() background: string = '#ccc';
  @Input() icon: string = 'star';

  public items: Array<number> = [1, 2, 3, 4, 5];

  private onTouchedCallback: () => void = () => { };
  private onChangeCallback: (_: any) => void = () => { };

  constructor() { }

  ngAfterViewInit() {
    this.items = Array(Number(this.stars));
    this.onChangeCallback(this.value);
  }

  setStar(star) {
    if (star + 1 == 1 && this.value == 1)
      this.value = 0;
    else if (star + 1 == 0)
      this.value = 1;
    else
      this.value = star + 1;

    this.change.emit(this.value);
    this.onTouchedCallback();
    this.onChangeCallback(this.value);
  }

  //From ControlValueAccessor interface
  writeValue(value: number) {
    if (value && Number(value) !== this.value) {
      this.value = Number(value);
      this.onChangeCallback(this.value);
    }
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
}

@NgModule({
  declarations: [RatingComponent],
  exports: [RatingComponent]
})
export class RatingComponentModule {}
